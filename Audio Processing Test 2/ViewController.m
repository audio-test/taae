//
//  ViewController.m
//  Audio Processing Test 2
//
//  Created by Stanislav Sidelnikov on 21/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

#import "ViewController.h"
#import <AEAudioController.h>
#import <AERecorder.h>
#import <AEAudioFilePlayer.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (nonatomic, strong) AEAudioController *audioController;
@property (nonatomic, strong) AERecorder *recorder;
@property (nonatomic, strong) AEAudioFilePlayer *player;
@property (nonatomic, strong) AEAudioUnitFilter *filter;

@property (nonatomic, strong) NSString *filePath;

- (void)setupFilter;

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  NSString *documentsFolder = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  self.filePath = [documentsFolder stringByAppendingPathComponent:@"Recording.aiff"];

  [self setupFilter];

  self.audioController = [[AEAudioController alloc] initWithAudioDescription:AEAudioStreamBasicDescriptionNonInterleaved16BitStereo inputEnabled:YES];
}

- (void)setupFilter {
  AudioComponentDescription component = AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_Effect, kAudioUnitSubType_Reverb2);
  self.filter = [[AEAudioUnitFilter alloc] initWithComponentDescription:component];
}

- (IBAction)recordTapped:(UIButton *)sender {
  if (!self.recorder) {
    if (self.player) {
      [self.audioController removeChannels:@[self.player]];
      self.player = nil;
    }
    self.recorder = [[AERecorder alloc] initWithAudioController:self.audioController];

    NSError *error = NULL;
    if (![self.recorder beginRecordingToFileAtPath:self.filePath fileType:kAudioFileAIFFType error:&error]) {
      NSLog(@"Error when starting recording: %@.", [error localizedDescription]);
      self.recorder = nil;
      return;
    }

    [self.audioController addInputReceiver:self.recorder];
    [self.audioController addOutputReceiver:self.recorder];
    [self.audioController addInputFilter:self.filter];

    [self.recordButton setTitle:@"Stop" forState:UIControlStateNormal];
    self.playButton.enabled = NO;
  } else {
    [self.audioController removeInputReceiver:self.recorder];
    [self.audioController removeOutputReceiver:self.recorder];
    [self.audioController removeFilter:self.filter];

    [self.recorder finishRecording];

    self.recorder = nil;

    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    self.playButton.enabled = YES;
  }
}

- (IBAction)playTapped:(UIButton *)sender {
  if (self.player) {
    [self.audioController removeChannels:@[self.player]];
    self.player = nil;
  }

  NSURL *fileUrl = [NSURL URLWithString:self.filePath];
  NSError *error = NULL;
  self.player = [AEAudioFilePlayer audioFilePlayerWithURL:fileUrl error:&error];
  if (error) {
    NSLog(@"Error initializing player: %@", [error localizedDescription]);
    self.player = nil;
    return;
  }
  self.player.loop = NO;
  self.player.removeUponFinish = YES;

  [self.audioController addFilter:self.filter toChannel:self.player];
  [self.audioController addChannels:@[self.player]];

}

@end
